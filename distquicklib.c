/*  distquicklib: library of concurrent and distributed quicksort algorithms 
	for COMP2310 Assignment 2, 2012.

Name: Timothy Sergeant 
StudentId:  U5006960

 ***Disclaimer***: (modify as appropriate)
 The work that I am submitting for this program is without significant
 contributions from others (excepting course staff).
 */

// uncomment when debugging. Example call: PRINTF(("x=%d\n", x));
//#define PRINTF(x) do { printf x; fflush(stdout); } while (0) 
#define PRINTF(x) /* use when not debugging */
//
//#DEFINE NDEBUG

#include <stdio.h>
#include <stdlib.h>  	/* malloc, free */
#include <strings.h> 	/* bcopy() */
#include <string.h>
#include <assert.h>  
#include <unistd.h>     /* fork(), pipe() */
#include <sys/types.h>
#include <sys/wait.h>   
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>

#include "quicklib.h"
#include "distquicklib.h"

void *runThread(void *s);

// distributed quick sort using pipes
void quickPipe(int A[], int n, int p) {
	int len, nbytes;
	int fd_cp[2];
	int status, piperesult;

	int m, childlength;
	if(p > 1) {
	    //Step case in the recursion: Fork the process,
	    //recurse on the subarrays and collect the results back together
		piperesult = pipe(fd_cp);
		if (piperesult != 0) {
			perror("Error occurred when creating pipe");
			exit(2);
		}
		m = partition(A, n);
		childlength = n - m - 1;

		PRINTF(("Chose partition at %d\n", m));
		len = sizeof(int)*childlength;

		PRINTF(("Splitting array of size %d, with partition %d\n", n, m));
		if (fork()) {
			/* PARENT */
			close(fd_cp[1]); /* close write cp */
			quickPipe(A, m + 1, p/2);
			
			// Get data for the other half of the array from the client
			int pos = 0, count;
			
			//We need to treat A as an array of bytes in order 
			//to receive data correctly
			char *right_start = (char *) &A[m+1];
			while (pos < len) {
				count = read(fd_cp[0], &right_start[pos], len);
				if(count == -1) {
					perror("PARENT - Error occurred while \
					reading from pipe");
					break;
				}
				pos += count;
			}
			assert(pos == len);


			PRINTF(("PARENT - nbytes=%d\n", nbytes));
			close(fd_cp[0]);
			wait(&status);
		} else {
			/* CHILD */
			close(fd_cp[0]); /* close read  cp*/

			//Call quickPipe on the righthand side of the array
			//By the time this call returns, it will be sorted correctly
			
			quickPipe(&A[m+1], childlength, p/2);

			nbytes = write(fd_cp[1], &A[m+1], len);
			assert(nbytes == len);
			PRINTF(("CHILD  - nbytes=%d\n", nbytes));
			close(fd_cp[1]);
			exit(0);
		}
	}
	else {
	    //Base case: Sort the given array
		PRINTF(("Sorting array of size %d\n", n));
		quickSort(A, n);
	}
} //quickPipe()

// distributed quick sort using sockets
void quickSocket(int A[], int n, int p) {
    int	len, nbytes;
	int status;

	int sock_serv, sock_client;
	struct sockaddr_in server;
	socklen_t addrLength;

	int m, childlength;
	if(p > 1) {
	    //Step case in the recursion: Fork the process,
	    //recurse on the subarrays and collect the results back together
		m = partition(A, n);
		childlength = n - m - 1;

		PRINTF(("Chose partition at %d\n", m));
		len = sizeof(int)*childlength;

		PRINTF(("Splitting array of size %d, with partition %d\n", n, m));

		/* Socket Prep */
		sock_serv = socket(AF_INET, SOCK_STREAM, 0);
		if(sock_serv == -1) {
			perror("Socket could not be created");
			exit(2);
		}

		server.sin_family = AF_INET;
		server.sin_port = 0;
		server.sin_addr.s_addr = INADDR_ANY;

		status = bind(sock_serv, (struct sockaddr *) &server, sizeof(server));
		if(status == -1) {
			perror("Could not bind server socket");
			exit(2);
		}

		addrLength = sizeof(server);
		//Get the socket address for the server. This is used to connect the child
		//to the correct port.
		if(getsockname(sock_serv, (struct sockaddr *) &server, &addrLength)) {
			perror("Could not find port number for server");
			exit(2);
		}

		//We are ready to fork

		if (fork()) {
			/* PARENT */
			
			//Create connection to child
			status = listen(sock_serv, 1);
			if (status == -1) {
				perror("Could not set server to listen");
			}
			PRINTF(("PARENT - accepting connection\n"));
			sock_client = accept(sock_serv, NULL, NULL);
			if(sock_client < 0) {
				perror("Accepting a client connection failed");
				exit(2);
			}

			//Sort half the array
			quickSocket(A, m + 1, p/2);

			// Get data for the other half of the array from the client
			int pos = 0, count;
			
			//We need to treat A as an array of bytes in order 
			//to receive data correctly
			char *right_start = (char *) &A[m+1];
			while (pos < len) {
				count = recv(sock_client, &right_start[pos], len - pos, 0);
				if(count == -1) {
					perror("PARENT - Error occurred while \
					reading from socket");
					break;
				}
				pos += count;
			}

			PRINTF(("PARENT - received=%d\n", pos));
			
			assert(pos == len);

			close(sock_client);
			close(sock_serv);
			int waitresult = wait(&status);
			if (waitresult == -1) {
				perror("Error while waiting for child process");
			}
		} else {
			/* CHILD */
			close(sock_serv); /* close server socket */
			//Create connection to server
			sock_client = socket(AF_INET, SOCK_STREAM, 0);
			if(sock_client < 0) {
				perror("Could not create client socket");
				exit(2);
			}
			PRINTF(("CHILD - Connecting to parent\n"));
			status = connect(sock_client, (struct sockaddr *) &server,
			                 sizeof(server));
			if (status == -1) {
				perror("Error connecting from child to parent");
				exit(2);
			}

			//Call quickSocket on the righthand side of the array
			//By the time this call returns, it will be sorted correctly
			quickSocket(&A[m+1], childlength, p/2);

			// All we need to do is transfer that data to the parent			
			nbytes = send(sock_client, &A[m+1], len, 0);

			PRINTF(("CHILD  - nbytes=%d\n", nbytes));
			assert(nbytes == len);

			close(sock_client);
			exit(0);
		}
	}
	else {
	    //Base case
		PRINTF(("Sorting array of size %d\n", n));
		quickSort(A, n);
	}
} //quickSocket()

// concurrent quick sort using pthreads 

static int *A;
static int totalThreads;
static enum WaitMechanismType waitMech;

//For WAIT_MEMLOC 
static volatile char *p_memlocks;
//For WAIT_MUTEX
static pthread_mutex_t *mutexes;

void quickThread(int *pA, int pn, int p, enum WaitMechanismType pWaitMech) {
	int i, status;
	A = pA;
	totalThreads = p;
	waitMech = pWaitMech;

    //Prepare the wait mechanism
	if(waitMech == WAIT_MEMLOC) {
		p_memlocks = (char *)malloc(p);
		if(p_memlocks == NULL) {
			perror("Error while reserving memory for memlocks");
			exit(2);
		}
		for(i = 0; i < p; i++) {
			p_memlocks[i] = (char)1;
		}
	} else if (waitMech == WAIT_MUTEX) {
		mutexes = (pthread_mutex_t *)malloc(p * sizeof(pthread_mutex_t));
		for(i = 0; i < p; i++) {
			status = pthread_mutex_init(&mutexes[i], NULL);
			if(status == -1) {
				perror("Could not create mutex");
				exit(2);
			}
		}
	}

    //Run the algorithm, seeding it with the arguments for thread 0
	int args[5] = { 0, 0, 0, pn, 1};
	runThread((void *) args);

    //Clean up the wait mechanism
	if(waitMech == WAIT_MEMLOC) {
		free((void *)p_memlocks);
	} else if(waitMech == WAIT_MUTEX) {
		free(mutexes);
	}
} //quickThread()


void *runThread(void *s) {
	int *args = (int *)s;
	int id = args[0];
	//Whether this thread will be finished when this method returns'
	//Used to prevent parent threads from being marked as finished
	//when they recurse down the tree.
	int alertOnTerm = args[1]; 
	int start = args[2];
	int length = args[3];
	int p = args[4];

	PRINTF(("%d: Starting threaded sort with start=%d, length=%d, p=%d\n", 
	            id, start, length, p));

	//If we are using mutexes, the thread must lock as soon as it starts
	if(alertOnTerm && waitMech == WAIT_MUTEX) {
		pthread_mutex_lock(&mutexes[id]);
	}

	if (p == totalThreads) {
	    //Base case
		quickSort(&A[start], length);
	}
	else {
	    //Step case
		int m = partition(&A[start], length);
		pthread_t child;

		int childId = id + p;
		int args[5] = {childId, 1, start + m + 1, length - m - 1, p * 2};

		pthread_create(&child, NULL, runThread, (void *) args);

		//Recurse for the left hand side
		int parent_args[5] = {id, 0, start, m + 1, p * 2};
		runThread((void *) parent_args);

        //Wait for the child
		if(waitMech == WAIT_JOIN) {
			pthread_join(child, NULL);
		}
		else if(waitMech == WAIT_MEMLOC) {
			while(p_memlocks[childId] == 1) 
				{/* Do nothing */}
		}
		else if(waitMech == WAIT_MUTEX) {
			pthread_mutex_lock(&mutexes[childId]);
		}
	}

	if(alertOnTerm && waitMech != WAIT_JOIN) {
		if(waitMech == WAIT_MEMLOC) {
			//Mark this thread as ready
			p_memlocks[id] = 0;	
		} else if (waitMech == WAIT_MUTEX) {
			//Unlock this thread's mutex
			pthread_mutex_unlock(&mutexes[id]);
		}
	}	

	return NULL;
}
