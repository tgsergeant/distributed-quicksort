/* ---------------------------------------------------------------------
   report.txt: report for comp2310 assignment 2, 2012
     Name: Timothy Sergeant
     Student Number: U5006960

***Disclaimer***: (modify as appropriate)
  The work that I am submitting for this program is without significant       
  contributions from others (excepting course staff).
*/

--------------------------------------------------------------------- */

1. Quicksocket failed for input sizes of around 35,000 integers and up,
   depending on the random data. Linux maintains a buffer which stores the
   information sent through the socket. The exact size of this buffer 
   fluctuates depending on load, with what appeared to be a default size
   of 65536 bytes. With sufficiently large input, a call to recv() would
   only retrieve the first section of the data sent by the child process.
   
   To fix this problem, a loop was created around the recv() call so that
   the data is pulled repeatedly from the socket until the expected amount
   has been received.

2. Five trials were taken with n=8000000 and p=4 and, with the best result 
   for each sort method recorded below:
      tim@ubuntu$ ./quicksort -p 8000000 4 | grep time
      elapsed time for sort: 0.256242s

      tim@ubuntu$ ./quicksort -s 8000000 4 | grep time
      elapsed time for sort: 0.243528s

      tim@ubuntu$ ./quicksort -t 8000000 4 | grep time
      elapsed time for sort: 0.202423s

   The same process was repeated with p=1:
      tim@ubuntu$ ./quicksort -p 8000000 1 | grep time
      elapsed time for sort: 0.525630s

      tim@ubuntu$ ./quicksort -s 8000000 1 | grep time
      elapsed time for sort: 0.525683s

      tim@ubuntu$ ./quicksort -t 8000000 1 | grep time
      elapsed time for sort: 0.525453s

   quickThread() is noticeably faster than the other sorting methods with
   n=4. This seems logical, considering how this implementation works:
   It is the only one of the three which can use shared memory and so does
   not explicitly need to transfer results between processes.

   quickThread() experiences the most speed-up when comparing n=1 to n=4
   (4 threads makes the sort approximately 2.6 times faster). This is
   quite a difference from the maximum possible speed-up of 4 times. A major
   reason for this difference would be the overhead associated with creating
   new threads (and processes), as well as the overhead of transferring data
   between processes, in the case of quickPipe and quickSocket.

   Another possible reason for the less-than-perfect speed-up with 4 threads 
   is that all four CPU cores are not being completely utilised. 
   When the number of threads is doubled to 8, elapsed time to sort 8 million
   elements is approximately 0.175 seconds, increasing the speed-up 
   to around 3 times. This suggests that all cores are now being used 
   effectively, as time has decreased despite the increase in overhead 
   from the additional threads.

4. The wait_on_memlock scheme works because each thread only needs to
   write to a single value in the array of locks. This guarantees that
   there will be no interference from other threads leading to the
   parent not waiting correctly on the child.

   The volatile keyword marks that the variable p_memlocks can be modified
   by multiple processes. This prevents the compiler from optimising access
   to p_memlocks, such as by changing
      while(p_memlocks[childID] == 1)
   to
      while(1)
   This can be seen by removing the volatile keyword and changing to compiler
   optimisation level. With -O2, the program will deadlock while waiting
   for child threads, but without compiler optimisation, the program runs 
   correctly.

5. The fastest times for the three variants are as follows:
      tim@ubuntu$ ./quicksort -t -v 0 8000 4 | grep time
      elapsed time for sort: 0.000964s

      tim@ubuntu$ ./quicksort -t -v 1 8000 4 | grep time
      elapsed time for sort: 0.000947s

      tim@ubuntu$ ./quicksort -t -v 2 8000 4 | grep time
      elapsed time for sort: 0.000938s
      
   These results indicate that memlock was fastest, followed by mutexlock,
   meaning that the optimisation was successful. However, at this scale of
   less than 1 millisecond, it is difficult to be certain about the timing.

6. With p=32, the following results were obtained:
      tim@ubuntu$ ./quicksort -t -v 0 8000 32 | grep time
      elapsed time for sort: 0.002151s

      tim@ubuntu$ ./quicksort -t -v 1 8000 32 | grep time
      elapsed time for sort: 0.001364s
   (Note that the majority of mutexlock results were very similar in
    performance to wait_on_join)

      tim@ubuntu$ ./quicksort -t -v 2 8000 32 | grep time
      elapsed time for sort: 0.058266s

   wait_on_mutexlock is now the fastest variant, followed by wait_on_join.
   With significantly more threads than CPUs, the busy wait loop in
   wait_on_memlock becomes very inefficient, as the scheduler will regularly
   make waiting threads active, increasing the number of context switches
   required. wait_on_join and mutexlock do not have this problem, as they
   wait at a system call level, explaining why their performance drops are
   less severe.
